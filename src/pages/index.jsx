import React from 'react';
import styled from "styled-components";

import Landing from "../components/landing";
import About from "../components/about";
import Features from "../components/features";
import Development from "../components/development";

import "./index.css";

export default () => (
	<div>
		<Landing />
		<About />
		<Features />
		<Development />
	</div>
)
