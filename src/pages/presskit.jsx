import React from 'react';
import styled from "styled-components"; 

export default ({data}) => (
    <Container dangerouslySetInnerHTML={{__html: data.markdownRemark.html}}>
    </Container>
);

const Container = styled.div`
    padding: 6vw 4vh;
    > p, li {
        font-size: 70%;
        font-family: "Roboto"
    }
    > h1 {
        font-size: 100%;
    }
    > ul li {
        margin: 2px 0;
    }
`;

export const content = graphql`
    query MarkdownContent {
        markdownRemark {
            html
        }
    }
`