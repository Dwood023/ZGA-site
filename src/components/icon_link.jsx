import React from "react";
import styled from "styled-components";

const IconLink = (props) => (
    <Box>
        <a href={props.url}>
            <Icon id="icon" className={props.fa_class} />
        </a>
        <Heading>{props.title}</Heading>
    </Box>
);

const Box = styled.div`
    text-align: center;
    z-index: 1;
`
const Icon = styled.i`
    color: white;
    &:hover {
        color: darkred;
    }
`
const Heading = styled.p`
    color: white;
    font-family: "Roboto";
    font-size: 0.6em;
    letter-spacing: 1px;
    padding: 0;
    margin: 0;
`
export default IconLink;