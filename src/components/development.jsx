import React from "react";
import styled from "styled-components";

export default () => (
    <Container>
        <a name="development"></a>
        <Title>DEVELOPMENT</Title>
        <Text>ZGA has been a solo, self-funded project of <a style={{color: "lightgray"}}href="mailto:zerogarenagame@gmail.com">Jon Wood</a>, with fantastic art contributions from the talented Max Graw.</Text> 
        <Text>Built in UE4 over many years of work, ZGA was greenlit on Steam and remains available in early access until its long-awaited full release on 08/08/18.</Text>
    </Container>
);

const Title = styled.h2`
    font-weight: normal;
    padding-bottom: 2vh;
`;

const Container = styled.div`
    padding: 2vh 20vw;
    text-align: center;
	color: white;
	background: black;
`;
const Tagline = styled.h3`
    font-family: Roboto;
`;
const Text = styled.p`
    font-size: 0.8rem;
`;
const Emph = styled.span`
	font-size: 0.8rem;
	font-weight: bold;
`;