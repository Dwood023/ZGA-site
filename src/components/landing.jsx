import React from "react";
import styled from "styled-components";

import Links from "./links";
import Countdown from "./countdown";
import logo from "../assets/logo.svg";
import mp4 from "../assets/zga_trailer.mp4";
import webm from "../assets/zga_trailer.webm";
import IconLink from "./icon_link";

const trailer_url = "https://www.youtube.com/watch?v=0__QxCCkQVM";

export default () => (
    <FullScreen>
        <a name="home"></a>
        <BackgroundImage>
            <Trailer autoPlay muted loop>
                <source src={webm} type="video/webm"/>
                <source src={mp4} type="video/mp4"/>
            </Trailer>
            <Logo src={logo} />
            <IconLink title="Watch Trailer" fa_class="fas fa-play-circle fa-5x" url={trailer_url}/>
        </BackgroundImage>
        <Overlay />
        <Footer>
            <Links />
            <Banner>
                <Title>RELEASE</Title>
                <Countdown />
                <h3>08/08/18</h3>
            </Banner>
        </Footer>
    </FullScreen>
)

const Trailer = styled.video`
    position: absolute;
    height: 90vh;
    padding: 0;
    left: 50%;
    transform: translate(-50%, 0%);
`;
const center_contents = `
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;
const background_size = `
    width: 100%;
    height: 90vh;
`;

const FullScreen = styled.div`
    /* To clip overflowing video */
    overflow-x: hidden; 
    position: relative;
    background-color: black;
    height: 100vh;
`;
const Logo = styled.img`
    z-index: 1;
`;
const Footer = styled.div`
    width: 100%;
    position: absolute;
    bottom: 0px;
`;
const BackgroundImage = styled.div`
    ${center_contents}
    ${background_size}
    z-index: -1;
`;

const Overlay = styled.div`
    ${background_size}
	background: radial-gradient(ellipse closest-corner, white 0%, rgba(0,0,0,0) 30%, rgba(0,0,0,1) 90%); 
    position: absolute;
    top: 0;
`

const Banner = styled.div`
    ${center_contents}
    padding: 2vh 4vw;
    flex-direction: column;
    color: white;
    background-color: black;
`;

const Title = styled.h1`
    font-size: 2em;
    font-weight: bold;
`;