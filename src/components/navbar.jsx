import React from "react";
import styled from "styled-components";
import logo_img from "../assets/zga_32.png";

export default () => (
    <Bar>
        <Logo src={logo_img}/>
        <Link href="/#home" style={{color: "red"}}>Home</Link>
        <Link href="/#about">About</Link>
        <Link href="/#features">Features</Link>
        <Link href="/#development">Development</Link>
        <Link href="/presskit" style={{color:"gold"}}>Presskit</Link>
    </Bar>
);

const Logo = styled.img`
    margin: 0;
`;
const Link = styled.a`
    font-size: 0.75rem;
    color: white;
    text-decoration: none;
    font-family: Exo;
    text-transform: uppercase;
`;
const Bar = styled.div`
    padding: 0.5vh 10vw;
    width: 100vw;
    display: flex;
    justify-content: space-between;
    color: white;
    position: fixed;
    top: 0;
    z-index:3;
    background: linear-gradient(rgba(10,10,10,0.9), rgba(20,20,20,0.6));
`;