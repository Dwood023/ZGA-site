import React from "react";
import styled from "styled-components";

class Gallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            i: 0,
        };
        this.cycle = this.cycle.bind(this);
    }

    cycle(n, e) {
        const length = this.props.children.length;
        const current = this.state.i;

        const i = current + n < 0 ? length + n : (n + current) % length;

        this.setState({
            i
        })
    }
    render() { 
        return (
            <Container>
                <Title>
                    <h2>{this.props.title}</h2>
                </Title>
                {this.props.children[this.state.i]}
                <Prev onClick={e => this.cycle(-1)}>
                    <i className="fas fa-arrow-left fa-5x" />
                </Prev>
                <Next onClick={e => this.cycle(1)}>
                    <i className="fas fa-arrow-right fa-5x" />
                </Next>
            </Container>
        )
    }
};

const Title = styled.div`
    position: absolute;
    width: 100%;
    text-align: center;
    padding: 1vh 0;
    margin: 0;
    color: white;
    background-color: rgba(0,0,0,0.5);
`;

const Arrow = styled.i`
    color: rgba(255,255,255,0.5);
    position: absolute;
    top: 50%;
    &:hover {
        color: white;
    }
`;
const Prev = Arrow.extend`
    left: 2%;
`;
const Next = Arrow.extend`
    right: 2%;
`;

const Container = styled.div`
    position: relative;
    width: 100%;
    height: 80vh;
    text-align: center;
    z-index: 1;
`;

export default Gallery;