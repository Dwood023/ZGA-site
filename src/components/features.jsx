import React from "react";
import styled from "styled-components";
import Gallery from "./gallery";

import move from "../assets/move.jpg";
import level from "../assets/level.jpg";
import ragdoll from "../assets/ragdoll.jpg";

export default () => (
    <div>
        <a name="features"></a>
        <Gallery title="FEATURES">
            <Feature title="Powerful movement system" img={move}>
                <Text>
                    Combine jumping, sliding, swinging abilities to dodge and run circles around opponents. Be quick, these weapon drops are a hot seller.
                </Text>
            </Feature>
            <Feature title="Omni-directional level design" img={level}>
                <Text>
                    Magnetic surfaces take the fighting to a full 360 degrees. Take to the walls and ceilings and ambush your enemies from any direction.
                </Text>
            </Feature>
            <Feature title="Ragdoll at will" img={ragdoll}>
                <Text>
                    Gravity getting you down? Turn your mag-boots off and re-invent parkour for outer space. Get creative, and you'll be bustin' moves like never before seen in a video game.
                </Text>
            </Feature>
        </Gallery>
    </div>
);

const Feature = ({children, title, img}) => (
    <Background img={img}>
        <Desc>
            <Tagline>{title}</Tagline>
            {children}
        </Desc>
    </Background>
);

const Desc = styled.div`
    position: absolute;
    padding: 3vh 25vw;
    text-align: center;
    color: white;
    width: 100%;
    bottom: 0;
    background: rgba(0,0,0,0.5);
`;

const Background = styled.div`
    background-image: ${props => `url(${props.img})`};
    box-shadow: inset 0 0 160px black;
	background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    width: 100%;
    height: 100%;
`;
const Tagline = styled.h3`
    font-family: Roboto;
`;
const Text = styled.p`
    font-size: 0.8rem;
`;