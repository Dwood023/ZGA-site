import React from 'react';
import ReactDOM from 'react-dom';

// return string of DD : HH : MM : SS
function time_left() {
    const now = new Date()
    // TODO -- Extract to argument
    const to = new Date("2018-08-08T18:00:00");

    var msecs_left = to - now;
    
    var units = [
        {
            name: "days",
            value: 0,
            conversion_factor: 1000 * 60 * 60 * 24
        },
        {
            name: "hours",
            value: 0,
            conversion_factor: 1000 * 60 * 60
        },
        {
            name: "minutes",
            value: 0,
            conversion_factor: 1000 * 60
        },
        {
            name: "seconds",
            value: 0,
            conversion_factor: 1000
        }/*,
        {
            name: "millisecs",
            value: 0,
            conversion_factor: 1
        }*/
    ];

    for (let unit of units) {
        // Get whole units
        unit.value = Math.floor(msecs_left / unit.conversion_factor);
        // Get remainder
        msecs_left -= (unit.value * unit.conversion_factor);
    }

    if (units.some(unit => unit.value < 0))
        return "OUT NOW";
    else if (units[0].value === 0)
        return "OUT TODAY";
    else 
        return units.map(unit => unit.value).join(" : ");
}
const styling = {
    fontSize: "3rem",
}

class Countdown extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            remaining: time_left()
        };
        this.update_delay = 10;
    }

    update() {
        this.setState({
            remaining: time_left()
        });
    }

    // Schedule updates
    componentDidMount() {
        this.interval_id = setInterval(
            () => this.update(),
            this.update_delay
        )
    }

    // Clean up task for browser
    componentWillUnmount() { 
        clearInterval(this.interval_id);
    }

    render() {
        return (
            <h1 id="countdown" style={styling}>
                {this.state.remaining}
            </h1>
        )
    }
}

export default Countdown;