import React from "react";
import styled from "styled-components";

export default () => (
    <Container>
        <a name="about"/>
        <Tagline>In space . . . No-one can votekick you for getting outrageously sicknasty midair railgun kills.</Tagline>
        <Text>Zero G Arena is finally bringing ragdoll physics to a deadly new forefront. Cartwheel, swing and slide through 3D battlegrounds at speeds that are quite literally, out of this world.</Text>
        <Text> Master <Emph> total freedom of movement </Emph> in 3-dimensions and <Emph> your friends will never know what fragged them. </Emph> </Text>
    </Container>
);

const Container = styled.div`
    padding: 2vh 20vw;
    text-align: center;
	background: black;
	color: white;
`;
const Tagline = styled.h3`
	font-family: Roboto;
	padding-bottom: 2vh;
`;
const Text = styled.p`
    font-size: 0.8rem;
`;
const Emph = styled.span`
	font-size: 0.8rem;
	font-weight: bold;
`;