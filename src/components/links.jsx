import React from "react";
import IconLink from "./icon_link";
import styled from "styled-components";

const twitter = "https://twitter.com/Zero_G_Arena";
const blog = "https://www.youtube.com/watch?v=GmkUUmxcT0Q&list=PLEn4T1_OqQNP89VSd-WPWJpAbCqIKAOWO";
const youtube = "https://www.youtube.com/channel/UC7nVXP23jd_NGW3V_ia7ppg";
const steam = "http://store.steampowered.com/app/467820/Zero_G_Arena/";
const facebook = "https://www.facebook.com/zerogarena/";

export default () => (
	<Links>
		<IconLink
			title="TWITTER"
			url={twitter}
			fa_class="fab fa-twitter fa-2x"
		/>
		<IconLink
			title="BLOG"
			url={blog}
			fa_class="fas fa-newspaper fa-2x"
		/>
		<IconLink
			title="STEAM"
			url={steam}
			fa_class="fab fa-steam fa-4x"
		/>
		<IconLink
			title="YOUTUBE"
			url={youtube}
			fa_class="fab fa-youtube fa-2x"
		/>
		<IconLink
			title="FACEBOOK"
			url={facebook}
			fa_class="fab fa-facebook fa-2x"
		/>
	</Links>
);

const to = "rgba(255,255,255,0)";
const from = "rgba(0,0,0,1)";

const Links = styled.div`
	display: flex;
    align-items: flex-end;
	justify-content: space-between;
	padding: 0 4vw;
	padding-top: 2vh;
	background: linear-gradient(${to},${from});
	background-clip: border-box;
`;