import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import './index.css'

import icon16 from "../assets/zga_16.png";
import icon32 from "../assets/zga_32.png";
import icon96 from "../assets/zga_96.png";


import Navbar from "../components/navbar";

const Layout = ({ children, data }) => (
  <div>
    <Helmet>
		<title>{data.site.siteMetadata.title}</title>

		{metadata}
		{icons}
		{google_stuff}
		{include_scripts}
    </Helmet>
	<Navbar />
	{children()}
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
const icons = [
	<link key={1} rel="shortcut icon" type="image/png" href={icon16} sizes="16x16"/>,
	<link key={2} rel="shortcut icon" type="image/png" href={icon32} sizes="32x32"/>,
	<link key={3} rel="shortcut icon" type="image/png" href={icon96} sizes="96x96"/>,
];

const google_stuff = [
	<meta key={1} name="google-site-verification" content="T1O40D9g7QSoeUhQ1TLCVONmRM_iLZa35Z-fcyYFdpE" />,
	<script key={2} async src="https://www.googletagmanager.com/gtag/js?id=UA-116664805-1"></script>,
	<script key={3}>
		{`
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-116664805-1');
		`}
	</script>,
];

const metadata = [
	<meta key={1} name='description' content='Official site for "Zero G Arena", the multiplayer zero-gravity shooter, available now for PC and Mac on Steam'/>,
	<meta key={2} name='keywords' content='Game, Arena, Shooter, Multiplayer, Zero-Gravity, Ragdolls, Game Physics, ZGA, Zero G Arena, 3D'/>
];

const include_scripts = [
	<script key={1} defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>,
];