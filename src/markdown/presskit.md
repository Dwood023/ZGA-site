---
title: "Presskit"
---
# Factsheet

**Developer:** Jonathan Wood  
**Release date:** 08/08/2018  
**Platforms:** Windows, Mac  
**Available from:** Steam: https://store.steampowered.com/app/467820/Zero_G_Arena/  
**Regular price:** $9.99 (will have a 40% week-long launch discount from the launch on 08/08/2018)  
# Contact

**Email:** mailto:zerogarenagame@gmail.com  
**Twitter:** https://twitter.com/Zero_G_Arena  
**Facebook:** https://www.facebook.com/zerogarena/  

# Game description

Zero G Arena is an online multiplayer competitive shooter featuring:

- Zero Gravity
- Ragdoll physics
- Grappling beams
- Magnet boots

You can toggle your magnet boots on and off at will to switch between running on magnetic surfaces (which can be at a variety of angles) or floating around, propelled by your grapple beam. Momentum is conserved between floating and running modes, so you can run to build up speed then jump and start flying in your last direction of travel. Combined with the grapple beam, this enables a near-endless range of stunts and maneuvers. 
A popular feature is the melee kick attack, which scales in damage with your speed, thus encouraging outrageous grapple beam and kick combination stunts.

The aforementioned magnetic surfaces can be at any angle, allowing for multiple “down” directions in a single map.

# Features

- Online multiplayer, with dedicated and listen (player hosted) servers
- LAN support
- Bots, for offline play, and to play with you on an empty server while you wait for others to join.
- Steam achievements
- Cosmetic options that are unlocked by Steam achievements, but otherwise free.
- 6 Game modes
- Community-made maps
- A maximum of 12 players per game for listen servers or 8 for dedicated servers
- Editable controls
- In-game text chat
- Detailed graphics settings.
- Audio, reticle and mouse settings
- 5 weapons (+ one alternative weapon that is now outdated and only features in a few maps).

# Development History

Zero G Arena was started as a solo project by myself, Jonathan Wood. Having developed an enthusiasm for programming on a physics foundation year (a one-year long course that allows admittance onto a physics degree), I spent the following summer holiday (2014) making a 2D top-down space game. It wasn't a very original game, but I learned a great deal about games programming and also had a lot of fun making it. By the end of the summer holiday, I reckoned I could make a commercially viable game, so I postponed my place on the course and shortly thereafter started work on what would become Zero G Arena. 

By about May 2015 the game had zero gravity, ragdoll physics, grappling beams and magnet boots, and appeared to have functioning online multiplayer. In my naivety I thought the bulk of the work was already done at this point and was vaguely intending to release it in the not-too-distant future. Also being quite confident about the game, I did not take up my deferred place on the physics degree that September. 

At some point between then and 2016 I lost confidence in the game, so applied for a computer science degree, not expecting to ever release Zero G Arena. Given the amount of work I'd put into already, I decided to at least put ZGA on Steam Greenlight and see what people thought of it. I continued to work on the game, despite not feeling very motivated and posted it to Greenlight on the 12th of April 2016. To my surprise, people on Greenlight really liked the idea of the game, even in it's very early state at the time and ZGA got through Greenlight very easily. Once again considering the possibility that ZGA could be successful, I tried to release playable version as quickly as possible, to determine whether people really would like the game in practice. 
On the 28th of July 2016, Zero G Arena launched in Steam Early Access for free. Once again, I was amazed by how positive the response was and how many people downloaded the game (despite having done no marketing). So many people were downloading the game at this point (about 10,000 a week), in fact, that technical support was getting unmanageable for a solo developer, with a game in an extremely early state. 

I had only made the game free because I didn't want to charge for a game that I didn't yet have much confidence in and might not develop to completion if it wasn't well liked.
So the dilemma now was whether to:

- Leave the game free, try and deal with the large numbers of players and add some kind of in-game monetisation later or 
- Make the game paid and stem the flow of players, but risk the game falling into insignificance. 

I opted for the later for the following reasons:
1. I don't particularly like in-game transactions of any kind. I prefer the system whereby you pay for a game, get the whole thing and it doesn't subsequently try to sell you anything.
2. My original plan had not been for the game to be extremely popular at this stage of development, just to assess whether people would like it and it would be worth completing. As such, if the number of players dropped off drastically following the game becoming paid, I would just act on feedback from a small playerbase to improve the game until it was ready for the full launch and then market the game to a wider audience.

In response to many requests by the player base, I added a system for community-made maps in February 2017. Following this I was particularly impressed by the work of a map maker called Max Graw (Ex3m on Steam) who I subsequently commissioned some art assets from and since early 2018 has been doing more major work on ZGA's graphics with more creative input. I've also been commissioning a lot of improved sound effects from a steam user and ZGA player, Deika.

Over the course of Early Access, the game has continued to yield an enthusiastic response from players and, with feedback from the community, I've continued to develop it into the polished and fully featured game that I initially aspired to make.

# Images

<a download href="all.zip">Download all</a>
![](ingame_1.jpg)
![](ingame_2.png)
![](ingame_3.png)
![](ingame_4.png)
![](ingame_5.png)
![](ingame_6.png)
![](ingame_7.png)
![](ingame_8.png)
![](ingame_9.png)

# Logos

![Full Logo](logo.svg)
![Round Logo](ZGA_logo_dark.png)
![Round Logo](ZGA_logo_white.png)
![Long Logo](ZGA_logo_website.png)

